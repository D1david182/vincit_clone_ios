
 ## Hello!
 ### Thanks for trying out the project.
 
 This project is a clone of the website https://www.vincit.com for the iPhone.
 The blog content is webscraped with a node.js script found in the root directory of this project and uploaded to a Firestore database.
 
 If you wish to run the script and update the database you will need to setup your own firestore database and supply the correct serviceAccount.json file which
 is retrieved from your firebase console. You will also have to change the GoogleService-Info.plist file in the xcode project to your own which is also found on your firebase console.
 
 To simply run the app, clone this repo and launch the **Vincit_iOS.xcworkspace** project. You will need xcode installed.
 
 #### If any errors occur you may need to:
 1. Run the command "pod install" in the project directory to get needed dependencies. 
 2. And possibly change the **Team** under "Signing" found on the Xcode project general settings page. 
 3. Email daniel.davidson.dev@gmail.com becuase he probably messed something up.

 You should be able to see an app consiting of blogs and various projects as shown below. Wow these images are much larger than I expected. 
 
![alt text](https://danielsrandom.s3.amazonaws.com/smartmockups_jym7biqk.png)


![alt text](https://danielsrandom.s3.amazonaws.com/smartmockups_jym7bxci.png)