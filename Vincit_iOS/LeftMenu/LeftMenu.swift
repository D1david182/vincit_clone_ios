//
//  LeftMenu.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit


protocol MenuDelegate : class {
    func handleShowMenu()
}

class MenuController : UIViewController {
    
    weak var delegate : MenuDelegate?
    let menuOptions = ["Blogs", "Projects"]
    let cellID = "cellID"
    
    lazy var tableView : UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.backgroundColor = vincitOrange
        table.separatorStyle = .none
        table.delegate = self
        table.dataSource = self
        return table
    }()
    lazy var header : MenuHeaderView = {
        let header = MenuHeaderView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 160))
        return header
    }()
    let subText : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Made By:\nDaniel Davidson :)"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 16, weight: .light)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = vincitOrange
        navigationController?.navigationBar.backgroundColor = vincitOrange
        
        
        view.addSubview(header)
        view.addSubview(tableView)
        view.addSubview(subText)
        
        header.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        header.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        header.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        header.heightAnchor.constraint(equalToConstant: 160).isActive = true
        
        tableView.topAnchor.constraint(equalTo: header.bottomAnchor).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: view.frame.height / 3).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        subText.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 30).isActive = true
        subText.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12).isActive = true
        subText.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12).isActive = true
        
    }
}

//////////////////TableViewController Delegate Functions//////////////////////////////////////
extension MenuController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MenuCell(style: .default, reuseIdentifier: cellID)
        cell.titleLabel.text = menuOptions[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    //When row is selected, close menu and show desired page
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.handleShowMenu()
        if menuOptions[indexPath.item] == "Projects"{
            navigationController?.pushViewController(ProjectsViewController(), animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
