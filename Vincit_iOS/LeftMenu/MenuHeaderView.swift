//
//  MenuHeaderView.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

class MenuHeaderView : UIView {
    
    lazy var logoImageView : UIImageView = {
        let imageview = UIImageView()
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.clipsToBounds = true
        imageview.contentMode = .scaleAspectFill
        imageview.isUserInteractionEnabled = true
        imageview.image = #imageLiteral(resourceName: "Icon-50")
        return imageview
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = vincitOrange
        translatesAutoresizingMaskIntoConstraints = false
        setupComponents()
    }
    
    fileprivate func setupComponents() {
        
        addSubview(logoImageView)
        
        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: frame.width / 5).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: frame.width / 5).isActive = true
        logoImageView.layer.cornerRadius = frame.width / 10
    
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
