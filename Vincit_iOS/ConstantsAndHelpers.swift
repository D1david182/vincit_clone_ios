//
//  Constants.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

let vincitOrange = UIColor(red:0.93, green:0.27, blue:0.17, alpha:1.0)

extension UIButton {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.15
        pulse.fromValue = 0.8
        pulse.toValue = 1.0
        pulse.autoreverses = false
        pulse.repeatCount = 0
        pulse.initialVelocity = 1
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: "pulse")
    }
}
