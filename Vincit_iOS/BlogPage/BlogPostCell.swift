//
//  BlogPostCell.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit


class BlogPostCell: UICollectionViewCell {
    
    var blogDetails : Blog? {
        didSet{
            //Set all label values when data arrives
            guard let blog = blogDetails else {return}
            titleLabel.text = blog.Title
            subTitleLabel.text = blog.SubTitle
            categoryLabel.text = " #" + blog.Category + "  "
            
            let attributedString = NSMutableAttributedString(string: blog.CreatorName, attributes: [.font:  UIFont.systemFont(ofSize: 18, weight: .medium), .foregroundColor: UIColor.black])
            let timeStamp = NSMutableAttributedString(string: " " + blog.TimeStamp, attributes: [.font:  UIFont.systemFont(ofSize: 15, weight: .medium), .foregroundColor: UIColor.lightGray])
            attributedString.append(timeStamp)
            posterName.attributedText = attributedString
            setCellColor(category: blog.Category)
        }
    }
    let categoryLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = vincitOrange
        lbl.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        lbl.backgroundColor = .white
        lbl.layer.cornerRadius = 15
        lbl.clipsToBounds = true
        return lbl
    }()
    let titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        lbl.numberOfLines = 0
        return lbl
    }()
    let subTitleLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 22, weight: .light)
        lbl.numberOfLines = 0
        return lbl
    }()
    let posterName : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        lbl.numberOfLines = 0
        return lbl
    }()

    lazy var smallPersonImage : UIImageView = {
        let imageview = UIImageView()
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.layer.cornerRadius = 11
        imageview.contentMode = .scaleAspectFill
        imageview.image = #imageLiteral(resourceName: "Icon-50")
        imageview.backgroundColor = vincitOrange
        imageview.clipsToBounds = true
        imageview.layer.borderColor = vincitOrange.cgColor
        imageview.layer.borderWidth = 3
        imageview.isUserInteractionEnabled = true
        return imageview
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        styleView()
        setupComponents()
    }
    //Add components to view and activate constraints
    private func setupComponents(){
        addSubview(categoryLabel)
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(smallPersonImage)
        addSubview(posterName)
        
        categoryLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4).isActive = true
        categoryLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        categoryLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        titleLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        subTitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 15).isActive = true
        subTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        subTitleLabel.bottomAnchor.constraint(equalTo: smallPersonImage.topAnchor, constant: -22).isActive = true
        
        smallPersonImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        smallPersonImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30).isActive = true
        smallPersonImage.heightAnchor.constraint(equalToConstant: 22).isActive = true
        smallPersonImage.widthAnchor.constraint(equalToConstant: 22).isActive = true
        
        posterName.leadingAnchor.constraint(equalTo: smallPersonImage.trailingAnchor, constant: 8).isActive = true
        posterName.centerYAnchor.constraint(equalTo: smallPersonImage.centerYAnchor).isActive = true
        posterName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
    }
    
    //Add shadow and color to cell background
    private func styleView(){
        backgroundColor = .white
        layer.cornerRadius = 6
        layer.shadowRadius = 3
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    }
    
    //Blub cells are colored red because they behave differently, most are video links
    private func setCellColor(category: String){
        if category == "Blurb"{
            backgroundColor = vincitOrange
            titleLabel.textColor = .white
            subTitleLabel.textColor = .white
            categoryLabel.textColor = UIColor.darkGray
            posterName.textColor = .white
        }else{
            backgroundColor = .white
            titleLabel.textColor = .black
            subTitleLabel.textColor = .black
            categoryLabel.textColor = vincitOrange
        }
    }
    deinit {
        blogDetails = nil
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
