//
//  CategoryCell.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

class CategoryCell: UICollectionViewCell {
    
    lazy var titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.darkGray
        lbl.font = UIFont.systemFont(ofSize: 18, weight: .light)
        lbl.textAlignment = .center
        return lbl
    }()
    //Toggle color when cell is selected
    override var isSelected: Bool{
        didSet{
            if isSelected{
                backgroundColor = vincitOrange
                titleLabel.textColor = .white
            }else{
                backgroundColor = .white
                titleLabel.textColor = UIColor.darkGray
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleLabel)
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        
        
        styleView(frame)
    }
    
    private func styleView(_ frame: CGRect) {
        layer.shadowRadius = 2
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        backgroundColor = .white
        clipsToBounds = false
        layer.cornerRadius = frame.height / 2
        layer.masksToBounds = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
