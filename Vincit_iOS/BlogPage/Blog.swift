//
//  Blog.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

//Used to store all blog details

struct Blog {
    let Title : String
    let SubTitle : String
    let CreatorName : String
    let TimeStamp : String
    let BlogURL : String
    let Category : String
    
    init(blog : [String : Any]) {
        self.Title = blog["title"] as? String ?? ""
        self.SubTitle = blog["subTitle"] as? String ?? ""
        self.CreatorName = blog["creatorName"] as? String ?? ""
        self.TimeStamp = blog["timeStamp"] as? String ?? ""
        self.BlogURL = blog["blogURL"] as? String ?? ""
        self.Category = blog["category"] as? String ?? ""
    }
}

