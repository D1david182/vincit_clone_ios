//
//  ViewController.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, MenuDelegate{

    let menuOpenVelocity : CGFloat = 500
    let menuWidth: CGFloat = 250
    let menuController = MenuController()
    let blurView = UIView()
    var menuIsShowing = false
    let cellID1 = "cellID1"
    let cellID2 = "cellID2"
    let categoriesArray = ["Design", "Culture", "Technology", "Blurbs"]
    var selectedCategory = "Design"
    let blogDataSource = BlogDataSource()
    
    //Reload collectionview when new data arrives
    var blogsArray = [Blog](){
        didSet{
            refreshControl.endRefreshing()
            mainFeedCollection.reloadData()
        }
    }

    lazy var leftMenuButton : UIBarButtonItem = {
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.addTarget(self , action: #selector(handleShowMenu), for: .touchUpInside)
        backButton.setImage(#imageLiteral(resourceName: "menu-three-horizontal-lines-symbol (2)").withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = .white
        let barButton = UIBarButtonItem(customView: backButton)
        NSLayoutConstraint.activate([(barButton.customView!.widthAnchor.constraint(equalToConstant: 30)),(barButton.customView!.heightAnchor.constraint(equalToConstant: 30))])
        return barButton
    }()
    lazy var rightMenuButton : UIBarButtonItem = {
        let logoButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        logoButton.setImage(#imageLiteral(resourceName: "Icon-50").withRenderingMode(.alwaysTemplate), for: .normal)
        logoButton.tintColor = .white
        let barButton = UIBarButtonItem(customView: logoButton)
        NSLayoutConstraint.activate([(barButton.customView!.widthAnchor.constraint(equalToConstant: 30)),(barButton.customView!.heightAnchor.constraint(equalToConstant: 30))])
        return barButton
    }()
    
    lazy var categoryCollection : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = .white
        collection.dataSource = self
        collection.delegate = self
        collection.alwaysBounceHorizontal = true
        collection.showsHorizontalScrollIndicator = false
        collection.register(CategoryCell.self, forCellWithReuseIdentifier: cellID2)
        return collection
    }()
    
    lazy var mainFeedCollection : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .white
        collection.alwaysBounceVertical = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.register(BlogPostCell.self, forCellWithReuseIdentifier: cellID1)
        collection.showsVerticalScrollIndicator = false
        collection.refreshControl = refreshControl
        return collection
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = vincitOrange
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Loading", attributes: [.font : UIFont.systemFont(ofSize: 10, weight: .light), .foregroundColor : UIColor.black])
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLeftMenu()
        setupPanGesture()
        setupDarkBlurView()
        styleNavBar()
        setupComponents()
        refreshControl.beginRefreshing()
        blogDataSource.retrieveBlogs {
            self.blogsArray = self.blogDataSource.getBlogsFor(category: self.selectedCategory)
        }
        categoryCollection.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
    }

    //Add components to the view and activate constraints
    private func setupComponents(){
        view.addSubview(categoryCollection)
        view.addSubview(mainFeedCollection)
        
        categoryCollection.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        categoryCollection.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        categoryCollection.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        categoryCollection.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        mainFeedCollection.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mainFeedCollection.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mainFeedCollection.topAnchor.constraint(equalTo: categoryCollection.bottomAnchor, constant: 20).isActive = true
        mainFeedCollection.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    //Custom nav bar styling
    private func styleNavBar(){
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 22, weight: .heavy)]
        navigationController?.navigationBar.backgroundColor = vincitOrange
        navigationController?.navigationBar.barTintColor = vincitOrange
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.title = "Vincit"
        navigationItem.leftBarButtonItem = leftMenuButton
        navigationItem.rightBarButtonItem = rightMenuButton
        view.backgroundColor = .white
    }
    
    //Add left menu as a subview to the HomeViewController
    private func setupLeftMenu() {
        menuController.view.frame = CGRect(x: -menuWidth, y: 0, width: menuWidth, height: self.view.frame.height)
        menuController.delegate = self
        let mainWindow = UIApplication.shared.keyWindow
        mainWindow?.addSubview(menuController.view)
        addChild(menuController)
    }
    
    //Add menu opening gesture recognizers
    private func setupPanGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleShowMenu))
        blurView.addGestureRecognizer(tapGesture)
        let panGesture2 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        blurView.addGestureRecognizer(panGesture2)
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        view.addGestureRecognizer(panGesture)
    }
    
    //Blur view used when left menu opens
    private func setupDarkBlurView(){
        blurView.backgroundColor = UIColor(white: 0, alpha: 0.2)
        blurView.alpha = 0
        blurView.isUserInteractionEnabled = false
        let mainWindow = UIApplication.shared.keyWindow
        mainWindow?.addSubview(blurView)
        blurView.frame = mainWindow?.frame ?? .zero
    }

    /////////////////Left Menu Functionality////////////////////////////////
    
    //Used to move pan menu open and closed
    @objc func handlePan(gesture: UIPanGestureRecognizer){
        let translation = gesture.translation(in: view)
        if gesture.state == .changed {
            var x = translation.x
            if menuIsShowing {
                x += menuWidth
            }
            x = min(menuWidth, x)
            x = max(0, x)
            let transform = CGAffineTransform(translationX: x, y: 0)
            menuController.view.transform = transform
            navigationController?.view.transform = transform
            blurView.transform = transform
            blurView.alpha = x / menuWidth
        }else if gesture.state == .ended{
            handleEndPanGesture(gesture: gesture)
        }
    }
    
    //Add animation to menu opening and closing
    private func performMenuAnimations(transform: CGAffineTransform){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.menuController.view.transform = transform
            self.navigationController?.view.transform = transform
            self.blurView.transform = transform
            self.blurView.alpha = transform == .identity ? 0 : 1
        })
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    //Toggle view ineraction ability while menu is open
    @objc func handleShowMenu(){
        if menuIsShowing == false{
            performMenuAnimations(transform: CGAffineTransform(translationX: self.menuWidth, y: 0))
            menuIsShowing = true
            blurView.isUserInteractionEnabled = true
        }else{
            performMenuAnimations(transform: .identity)
            menuIsShowing = false
            blurView.isUserInteractionEnabled = false
        }
    }
    //Handle snapping of menu open or closed
    private func handleEndPanGesture(gesture : UIPanGestureRecognizer){
        let translation = gesture.translation(in: view)
        let velocity = gesture.velocity(in: view)
        if menuIsShowing{
            //If velocity of swipe is high, just hide tab
            if abs(velocity.x) > menuOpenVelocity{
                menuIsShowing = true
                handleShowMenu()
                return
            }
            //If swipe is less than half of the menu width
            if abs(translation.x) < menuWidth / 2 {
                //keep open
                menuIsShowing = false
            }else{
                //close
                menuIsShowing = true
            }
            handleShowMenu()
        }else{
            //If velocity of swipe is high, just open tab
            if velocity.x > menuOpenVelocity{
                menuIsShowing = false
                handleShowMenu()
                return
            }
            //Same logic but closed at start
            if translation.x < menuWidth / 2 {
                //close
                menuIsShowing = true
            }else{
                //keep open
                menuIsShowing = false
            }
            handleShowMenu()
        }
    }
    
    /////////////////Helper Functions ////////////////////////////////
    @objc private func handleRefresh(){
        blogDataSource.retrieveBlogs {
            self.mainFeedCollection.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    private func openBlogWebPage(url: String){
        if let blogURL = URL(string: url) {
            UIApplication.shared.open(blogURL)
        }
    }
}


//////////////////CollectionView Delegate Functions//////////////////////////////////////
extension  HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollection{
            blogsArray = blogDataSource.getBlogsFor(category: categoriesArray[indexPath.item])
        }else{
            openBlogWebPage(url: blogsArray[indexPath.item].BlogURL)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollection{
            return categoriesArray.count
        }
        return blogsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //Autosize based on cells contents
        if collectionView == categoryCollection{
            let frame = CGRect(x: 0, y: 0, width: 80, height: 35)
            let dummyCell = CategoryCell(frame: frame)
            dummyCell.titleLabel.text = categoriesArray[indexPath.item]
            dummyCell.layoutIfNeeded()
            let targetSize = CGSize(width: frame.width - 10, height: 35)
            let estimatedSize = dummyCell.systemLayoutSizeFitting(targetSize)
            let width = max(40, estimatedSize.width)
            
            return CGSize(width: width + 5, height: 35)
        }
        let frame = CGRect(x: 0, y: 0, width: view.frame.width - 24, height: 200)
        let dummyCell = BlogPostCell(frame: frame)
        dummyCell.blogDetails = blogsArray[indexPath.item]
        dummyCell.layoutIfNeeded()
        let targetSize = CGSize(width: frame.width, height: frame.height)
        let estimatedSize = dummyCell.systemLayoutSizeFitting(targetSize)
        let height = max(frame.height, estimatedSize.height)
        
        return CGSize(width: frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == categoryCollection ? 5 : 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 7, left: 12, bottom: 7, right: 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //Return cell type varied on collectionview
        if collectionView == categoryCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID2, for: indexPath) as! CategoryCell
            cell.titleLabel.text = categoriesArray[indexPath.item]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID1, for: indexPath) as! BlogPostCell
        cell.blogDetails = blogsArray[indexPath.item]
        return cell
    }
}
