//
//  BlogDataSource.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/26/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import Firebase

class BlogDataSource: NSObject {
    
    var blogsArray = [Blog]()
    
    override init() {
        super.init()
        
    }
    //Allows user to filter by selected category
    func getBlogsFor(category: String) -> [Blog]{
        if category == "Blurbs"{
            return blogsArray.filter({$0.Category == "Blurb"})
        }
        return blogsArray.filter({$0.Category == category})
    }
    
    func getTotalBlogs() -> Int{
        return blogsArray.count
    }
    
    func getBlogOfCategoryAtIndex(index: Int) -> Blog{
        return blogsArray[index]
    }
    
    //Call to firestore and retrieve all blogs
    func retrieveBlogs(completion: @escaping () ->()){
        Firestore.firestore().collection("Blogs").getDocuments { (snapshot, error) in
            if let err = error{
                print("Error getting blogs", err)
                completion()
                return
            }
            guard let snapshot = snapshot else {return}
            let blogs = snapshot.documents
            self.blogsArray = blogs.map({ (blog) -> Blog in
                return Blog(blog: blog.data())
            })
            completion()
        }
    }
}
