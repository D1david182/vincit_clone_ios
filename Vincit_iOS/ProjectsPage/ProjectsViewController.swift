//
//  ProjectsViewController.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/27/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit
import CHIPageControl

class ProjectsViewController: UIViewController {
    
    var CellID = "CellID"
    let projectsArray = ProjectsDataSource().projectsArray
    
    lazy var backButton : UIBarButtonItem = {
        let btn = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBack))
        return btn
    }()
    
    lazy var mainCollection : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.register(PojectDetailCell.self, forCellWithReuseIdentifier: CellID)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.delegate = self
        collection.dataSource = self
        collection.showsHorizontalScrollIndicator = false
        collection.isPagingEnabled = true
        collection.backgroundColor = .white
        return collection
    }()
    
    lazy var pageIndicator : CHIPageControlPaprika = {
        let control = CHIPageControlPaprika()
        control.translatesAutoresizingMaskIntoConstraints = false
        control.set(progress: 0, animated: true)
        control.numberOfPages = projectsArray.count
        control.radius = 4
        control.tintColor = vincitOrange
        return control
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleNavBar()
        setupComponents()
    }
    
    private func styleNavBar(){
        navigationItem.title = "Projects"
        navigationItem.leftBarButtonItem = backButton
        view.backgroundColor = .white
    }
    
    func setupComponents(){
        view.addSubview(mainCollection)
        view.addSubview(pageIndicator)
        
        mainCollection.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        mainCollection.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mainCollection.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mainCollection.bottomAnchor.constraint(equalTo: pageIndicator.topAnchor, constant: -18).isActive = true
        
        pageIndicator.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -40).isActive = true
        pageIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    //Update the page indicator when scrollview starts to decelerate
    var currentIndex : CGFloat = 0.0
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        currentIndex = index
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.2) {
                self.pageIndicator.set(progress: Int(index), animated: true)
            }
        }
    }
    @objc private func handleBack(){
        navigationController?.popViewController(animated: true)
    }
}

//////////////////CollectionView Delegate Functions//////////////////////////////////////
extension ProjectsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projectsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID, for: indexPath) as! PojectDetailCell
        cell.projectDetails = projectsArray[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainCollection.frame.width, height: mainCollection.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
