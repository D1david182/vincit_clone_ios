//
//  ProjectsDataSource.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/27/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

class ProjectsDataSource: NSObject {
    
    var projectsArray = [Project]()
    
    override init() {
        super.init()
        addProjects()
    }
    
    //Hard coded values, could be web scraped in the future
    private func addProjects(){
        
        
        let proj1 = Project(Title: "Yamaha Watercraft App", ProjectURL: "https://www.vincit.com/case-study/yamaha-watercraft-apps", Image:  UIImage(named: "Yamaha"), Tags: ["Strategy", "UX", "UI", "Software Development"])
        
        let proj2 = Project(Title: "Kellogg's Experience Guidelines", ProjectURL: "https://www.vincit.com/case-study/kelloggs", Image: UIImage(named: "KLG_LaptopHero"), Tags: ["Strategy", "UX", "UI"])
        
        let proj3 = Project(Title: "Metso Metrics Service", ProjectURL: "https://www.vincit.com/case-study/metso-metrics-service", Image: UIImage(named: "Metso-Hero"), Tags: ["Strategy", "UX", "UI", "Web"])
        
        let proj4 = Project(Title: "KCRW App", ProjectURL: "https://www.vincit.com/case-study/kcrw-app", Image: UIImage(named: "kcrw-app"), Tags: ["Software Development"])
        
        let proj5 = Project(Title: "Stitch & Tie e-Commerce", ProjectURL: "https://www.vincit.com/case-study/stitch-and-tie", Image: UIImage(named: "suitsImage"), Tags: ["Strategy", "UX", "UI", "Web Development"])
        
        let proj6 = Project(Title: "College Confidential Search", ProjectURL: "https://www.vincit.com/case-study/college-search", Image: UIImage(named: "college_confidential"), Tags: ["Web Development"])
        
        let proj7 = Project(Title: "Superoperator App", ProjectURL: "https://www.vincit.com/case-study/superoperator-app", Image: UIImage(named: "superoperator-hero"), Tags: ["Strategy", "UX", "UI", "Software Development"])
        
        let proj8 = Project(Title: "Framery Configurator & App", ProjectURL: "https://www.vincit.com/case-study/framery-configurator-app", Image: UIImage(named: "Framery-Hero"), Tags: ["UX", "UI", "Software Development", "Web Development"])
        
        let proj9 = Project(Title: "Yepzon App", ProjectURL: "https://www.vincit.com/case-study/yepzon-app", Image: UIImage(named: "Yepzon-Hero"), Tags: ["UX", "UI", "Software Development"])
        
        let proj10 = Project(Title: "Dish App", ProjectURL: "https://www.vincit.com/case-study/dish-app", Image: UIImage(named: "Dish_Feature"), Tags: ["Strategy","UX", "UI", "Software Development"])
        
        projectsArray.append(proj1)
        projectsArray.append(proj2)
        projectsArray.append(proj3)
        projectsArray.append(proj4)
        projectsArray.append(proj5)
        projectsArray.append(proj6)
        projectsArray.append(proj7)
        projectsArray.append(proj8)
        projectsArray.append(proj9)
        projectsArray.append(proj10)
    }
}

struct Project {
    let Title : String
    let ProjectURL: String
    let Image : UIImage?
    let Tags : [String]
}
