//
//  PojectDetailCell.swift
//  Vincit_iOS
//
//  Created by Daniel Davidson on 7/27/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

class PojectDetailCell: UICollectionViewCell {
    
    var projectDetails : Project? {
        didSet{
            guard let project = projectDetails else {return}
            projectImage.image = project.Image
            titleLabel.text = project.Title
            
            //Changing color of '|' in string
            let seperatedString = project.Tags.joined(separator: " | ")
            tagsLabel.attributedText = seperatedString.reduce(NSMutableAttributedString()) {
                $0.append( NSAttributedString(string: String($1),attributes: [.foregroundColor: $1 == "|" ? vincitOrange: UIColor.darkGray]))
                return $0
            }
        }
    }
    
    let projectImage : UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFit
        img.backgroundColor = .white
        img.clipsToBounds = true
        return img
    }()
    let seperatorView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        return view
    }()
    
    let titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()
    
    let tagsLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 18, weight: .light)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        return lbl
    }()
    
    lazy var readMoreButton : UIButton = {
       let btn = UIButton()
        btn.setTitle("Read More", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        btn.backgroundColor = vincitOrange
        btn.layer.cornerRadius = 20
        btn.layer.shadowRadius = 3
        btn.layer.shadowColor = UIColor.darkGray.cgColor
        btn.layer.shadowOpacity = 0.2
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn.addTarget(self, action: #selector(handleShowArticle), for: .touchUpInside)
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupComponents()
    }
    
    private func setupComponents(){
        addSubview(projectImage)
        addSubview(seperatorView)
        addSubview(titleLabel)
        addSubview(tagsLabel)
        addSubview(readMoreButton)
        
        projectImage.topAnchor.constraint(equalTo: topAnchor).isActive = true
        projectImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 35).isActive = true
        projectImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -35).isActive = true
        projectImage.heightAnchor.constraint(equalToConstant: frame.width - 70).isActive = true
        
        seperatorView.topAnchor.constraint(equalTo: projectImage.bottomAnchor, constant: 20).isActive = true
        seperatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        seperatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        seperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: seperatorView.bottomAnchor, constant: 30).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        tagsLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        tagsLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        tagsLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        readMoreButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        readMoreButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        readMoreButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        readMoreButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
    }
    
    @objc private func handleShowArticle(sender : UIButton){
        sender.pulsate()
        guard let url = projectDetails?.ProjectURL else {return}
        if let blogURL = URL(string: url) {
            UIApplication.shared.open(blogURL)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
